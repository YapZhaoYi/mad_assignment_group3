package sg.edu.np.madTeam3.assignment.Activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import sg.edu.np.madTeam3.assignment.Models.UserModel;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import sg.edu.np.madTeam3.assignment.R;
import sg.edu.np.madTeam3.assignment.Utility.DBHandler;

import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class LoginActivity extends AppCompatActivity {
    /*
        Author: Zhao Yi
        Sections: All except @Delbert & @Yu Min

        Author: Delbert
        Sections: 1.1 (Validation & Firebase), 1.3, 2, 3, 4, 5, 6

        Author: Yu Min
        Sections: 1.4

        Login Activity
        Purpose: To add basic security to the application as one may store progress on important
                 projects or keep the planning of secret events accessible only to the user. The
                 user can also store multiple accounts for different purposes.
        Accessed from: This activity is accessed as the default activity or from a log out function
                       from Main Activity or from register Activity.

        1. OnCreate
            1.1 Login [Line 119]
                 A login function with basic validation and verification of input data,
                 and directs the user to the main activity.
            1.2 Register [Line 168]
                 A function to direct the user to create a new account through the register activity.
            1.3 Facebook Login [Line 179]
                 A login function using Facebook
            1.4 Change password [Line 207]
                 A function to change the user's password if they forgot it.
        2. Handle Facebook Token [Line 219]
            Check if user login is successful and get the unique token for the user
        3. Load User Profile [Line 237]
            Get the user details such as email and username. Store the user data
            and directs the user to the main activity
        4. OnActivityResult [Line 278]
            Get the result from Facebook call back manager activity
        5. OnStop [Line 286]
            Signs user out of FirebaseAuth and set Facebook token to null
        6. OnDestroy [Line 295]
            Signs user out of FirebaseAuth and set Facebook token to null
    */

    //Declare variables
    private EditText inputUsername, inputPassword;

    //For Facebook
    private CallbackManager mCallBackManager;
    private AccessTokenTracker accessTokenTracker;
    private FirebaseAuth mAuth;

    //Set reference to Database
    private DBHandler dbHandler = new DBHandler(this, "PrOrganize.db", null, 1);

    //1. OnCreate
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Create Firebase instance
        mAuth = FirebaseAuth.getInstance();
        //Initialize Facebook SDK
        FacebookSdk.sdkInitialize(getApplicationContext());

        //Set reference to XML
        inputUsername = findViewById(R.id.inputUserName);
        inputPassword = findViewById(R.id.inputPass);
        Button buttonLogin = findViewById(R.id.btnLogin);
        TextView textRegister = findViewById(R.id.textReg);
        LoginButton fbLoginButton = findViewById(R.id.fbLogin);
        TextView textForgetPassword = findViewById(R.id.textForgetPassword);

        //1.1 Login
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Declare function variables
                String username, password;

                //Get inputted text
                username = inputUsername.getText().toString().trim();
                password = inputPassword.getText().toString().trim();

                //Validation
                //Check if username is empty
                if (TextUtils.isEmpty(username)) {
                    inputUsername.setError("Name is required.");
                    return; //Exit function if true
                }

                //Check if password is empty
                if (TextUtils.isEmpty(password)) {
                    inputPassword.setError("Password is required.");
                    return; //Exit function if true
                }

                //Check if password is lesser than 6 characters
                if (password.length() < 6) {
                    inputPassword.setError("Password must be equal or greater than 6 characters");
                    return; //Exit function if true
                }

                //Verification
                //Check if user exists
                if (dbHandler.validateUser(username)) {
                    //If true, check if password matches user
                    if (dbHandler.authenticatePassword(username, password)) {
                        //If true, proceed to Main Activity
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("userData", username); //Required data to identify current user
                        startActivity(intent);
                        finish();
                    } else {
                        inputPassword.setError("Incorrect password");
                    }
                } else {
                    inputUsername.setError("User does not exist.");
                }
            }
        });

        //1.2 Register
        textRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Proceed to Register Activity
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });

        //1.3 Facebook Login
        //When clicking on facebook login button, call back is registered which gives success or failure result
        mCallBackManager = CallbackManager.Factory.create();
        //Setting permission to read user's facebook details
        fbLoginButton.setReadPermissions(Arrays.asList("public_profile", "email"));

        //Get response if log in is successful
        fbLoginButton.registerCallback(mCallBackManager, new FacebookCallback<LoginResult>() {
            //Success
            @Override
            public void onSuccess(LoginResult loginResult) {
                //Method to handle Facebook token
                handleFacebookToken(loginResult.getAccessToken());
            }

            //Cancel
            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Cancelled!", Toast.LENGTH_SHORT).show();
            }

            //Error
            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginActivity.this, "Error!" + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //1.4 Forget Password
        //Redirect user to Forget Password activity when "Forget Password" is clicked
        textForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    //2. Handle Facebook Token
    private void handleFacebookToken(final AccessToken token) {
        final AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(LoginActivity.this, "Please wait while we retrieve your account", Toast.LENGTH_SHORT).show();
                    //Method to get user name and email
                    //and proceed to main activity
                    loadUserProfile(token);
                } else {
                    Toast.makeText(LoginActivity.this, "Failed to logged in with credentials!" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //3. Load User Profile
    //Getting user details such as name and email
    private void loadUserProfile(AccessToken newAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(newAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    //Obtain user name and email
                    String name = object.getString("first_name");
                    String email = object.getString("email");
                    Intent intent;
                    //Checks if user exists
                    if (dbHandler.validateUser(name)) {
                        //if true, toast and pass data
                        Toast.makeText(LoginActivity.this, "Existing User. Successfully logged in with credentials!", Toast.LENGTH_SHORT).show();
                        intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("userData", name); //Required data to identify current user
                    } else {
                        //if false, toast and create new user
                        Toast.makeText(LoginActivity.this, "New User. Successfully logged in with credentials!", Toast.LENGTH_SHORT).show();
                        String pass = "";
                        UserModel userData = new UserModel(name, pass, email);
                        dbHandler.addUser(userData); //Add to database

                        //Proceed to Main Activity
                        intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("userData", name); //Required data to identify current user
                    }
                    startActivity(intent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name, email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    //4. OnActivityResult
    //Get result from facebook callbackmanager
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        mCallBackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    //5. OnStop
    //Signs user out and set facebook token to null
    @Override
    protected void onStop() {
        FirebaseAuth.getInstance().signOut();
        AccessToken.setCurrentAccessToken(null);
        super.onStop();
    }

    //6. OnDestroy
    //Signs user out and set facebook token to null
    @Override
    protected void onDestroy() {
        FirebaseAuth.getInstance().signOut();
        AccessToken.setCurrentAccessToken(null);
        super.onDestroy();
    }
}
