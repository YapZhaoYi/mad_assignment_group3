package sg.edu.np.madTeam3.assignment.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;


import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import sg.edu.np.madTeam3.assignment.Fragments.HomeFragment;
import sg.edu.np.madTeam3.assignment.Fragments.PomodoroFragment;
import sg.edu.np.madTeam3.assignment.Fragments.ProjectFragment;
import sg.edu.np.madTeam3.assignment.Models.ProjectModel;
import sg.edu.np.madTeam3.assignment.R;
import sg.edu.np.madTeam3.assignment.Models.UserModel;
import sg.edu.np.madTeam3.assignment.Utility.DBHandler;
import sg.edu.np.madTeam3.assignment.Utility.ImageByteConverter;

import com.facebook.AccessToken;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    /*
        Author: Zhao Yi
        Sections: All except @Delbert

        Author: Delbert
        Section: 3.4 (Firebase)

        Main Activity
        Purpose: To access all other app functionality, to set up a navigation drawer.
        Accessed from: This activity is accessed from Login Activity and Register Activity.

        1. OnCreate
        2. OnBackPressed
            Pressing back will now close drawer or display home fragment.
        3. Choose navigation item
            3.1 Navigate home [Line 134]
                 Runs section 4, display home fragment.
            3.2 Navigate pomodoro [Line 139]
                 Display pomodoro fragment, updates action bar title
            3.3 Navigate add project [Line 148]
                 Adds a new project and refreshes the navigation drawer.
            3.4 Navigate log out [Line 164]
                 Signs out from Facebook and proceeds to login Activity.
            3.5 Navigate project [Line 174]
                 Display project fragment with project details.
        4. Display home fragment [Line 200]
            Updates action bar title.
        5. Populate project menu options [Line 224]
            Gets all user projects and inserts into menu.
    */

    //Declare variables
    private DrawerLayout drawer;
    private UserModel userData;
    private NavigationView navView;
    public MenuItem mPreviousMenuItem; //Will be referenced by other redirect functions in priorityTaskAdapter and TaskFragment

    //Set reference to database
    private DBHandler dbHandler = new DBHandler(this, "PrOrganize.db", null, 1);

    //1. OnCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Get required data
        Intent intent = getIntent();
        userData = dbHandler.getUser(intent.getStringExtra("userData"));

        //Set up toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Set up navigation view
        //Set reference to XML
        drawer = findViewById(R.id.drawer_layout);
        navView = findViewById(R.id.nav_view);

        //Open and close drawer
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //Select navigation view items
        navView.setNavigationItemSelectedListener(this);
        populateWithProjects(); //Fills project submenu with projects

        //Display default home fragment if no saved state
        if (savedInstanceState == null) {
            defaultNavigation();
        }
    }

    //2. OnBackPressed
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START); //close drawer
        } else {
            defaultNavigation(); //return to home fragment as long as back is pressed
        }
    }

    //3. Choose navigation item
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        //Set navigation item checked if not "Add project"
        if (item.getItemId() != R.id.nav_add) {
            item.setChecked(true);

            //If there is a previous navigation item
            if (mPreviousMenuItem != null) {
                mPreviousMenuItem.setChecked(false); //Uncheck previous navigation item
            }

            //Set reference to current navigation item
            mPreviousMenuItem = item;
        }

        //Run selected navigation item process
        switch (item.getItemId()) {
            case R.id.nav_home: //3.1 Navigate home
                //Display home fragment
                defaultNavigation();
                break;

            case R.id.nav_pomodoro: //3.2 Navigate pomodoro
                //Display pomodoro fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new PomodoroFragment()).commit();

                //Change actionbar title
                getSupportActionBar().setTitle("Pomodoro");
                break;

            case R.id.nav_add: //3.3 Navigate add project
                //Add new project to database
                dbHandler.addProjectToUser(userData.getName(), "New Project", null);

                //Refresh navigation menu with new project
                navView.getMenu().clear();
                navView.inflateMenu(R.menu.drawer_menu);
                populateWithProjects();

                //Complete add message
                Toast.makeText(this, "Project Added Successfully", Toast.LENGTH_SHORT).show();
                break;

            case R.id.nav_submenu_title: //To use the default case for projects only
                break;

            case R.id.nav_logOut: //3.4 Navigate log out
                //Logging out of Firebase
                FirebaseAuth.getInstance().signOut();
                //Setting Facebook token to null
                AccessToken.setCurrentAccessToken(null);
                //Proceed to login activity
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();

            default: //3.5 Navigate project
                if (userData.getProjects() != null){
                    //Find project selected
                    for (ProjectModel project : userData.getProjects()) {
                        if (project.getProjectId().equals(item.getItemId())) {

                            //Display project fragment
                            Bundle bundle = new Bundle();
                            bundle.putInt("projectId", project.getProjectId()); //Required data to identify project
                            ProjectFragment projectObj = new ProjectFragment();
                            projectObj.setArguments(bundle);
                            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                    projectObj).commit();

                            //Change actionbar title
                            getSupportActionBar().setTitle("Projects");
                            break;
                        }
                    }

                    break;
                }
        }

        drawer.closeDrawer(GravityCompat.START);
        return true; //item is selected after clicked
    }

    //4. Display home fragment
    public void defaultNavigation() {
        //Display home fragment
        Bundle userBundle = new Bundle();
        userBundle.putString("username", userData.getName()); //Required data to identify user
        HomeFragment homeObj = new HomeFragment();
        homeObj.setArguments(userBundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                homeObj).commit();

        //Change actionbar title
        getSupportActionBar().setTitle("Home");

        //If there is a previous navigation item
        if (mPreviousMenuItem != null) {
            mPreviousMenuItem.setChecked(false); //Uncheck previous navigation item
        }

        //Set reference to current navigation item
        mPreviousMenuItem = navView.getMenu().findItem(R.id.nav_home);

        mPreviousMenuItem.setChecked(true);
    }

    //5. Populate project menu options
    public void populateWithProjects() {

        Menu menu = navView.getMenu();
        Menu submenu = menu.getItem(4).getSubMenu(); //Get "Add projects" submenu

        //Get all projects
        userData.setProjects(dbHandler.getAllUserProjects(userData));

        navView.setItemIconTintList(null); //To prevent images from appearing as a default gray box tint

        //Insert projects as menu options
        if (userData.getProjects() != null && !userData.getProjects().isEmpty()) {
            for (int i = 0; i < userData.getProjects().size(); i++) {
                ProjectModel project = userData.getProjects().get(i);

                //Check if project has a project icon
                if (project.getImage() == null) {
                    //If false, set default image
                    submenu.add(Menu.NONE, project.getProjectId(), i, project.getTitle()).setIcon(R.drawable.ic_project_default).setCheckable(true);
                } else {
                    //If true, set project icon
                    submenu.add(Menu.NONE, project.getProjectId(), i, project.getTitle()).setIcon(new BitmapDrawable(getResources(), ImageByteConverter.getImage(project.getImage()))).setCheckable(true);
                }
            }
        }

        //set navView to be refreshed
        navView.invalidate();

    }
}
