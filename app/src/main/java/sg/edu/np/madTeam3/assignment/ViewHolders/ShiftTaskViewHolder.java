package sg.edu.np.madTeam3.assignment.ViewHolders;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import sg.edu.np.madTeam3.assignment.R;

public class ShiftTaskViewHolder extends RecyclerView.ViewHolder {
    /*
        Author: Zhao Yi
        Sections: All


        ShiftTaskViewHolder
        Purpose: To display a single category to be selected to shift a task to.
    */

    public TextView selectCategoryName;

    public ShiftTaskViewHolder(@NonNull View itemView) {
        super(itemView);

        selectCategoryName = itemView.findViewById(R.id.selectCategoryName);
    }
}
