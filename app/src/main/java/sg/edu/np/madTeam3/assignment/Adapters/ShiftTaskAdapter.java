package sg.edu.np.madTeam3.assignment.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import sg.edu.np.madTeam3.assignment.Activities.MainActivity;
import sg.edu.np.madTeam3.assignment.Fragments.ProjectFragment;
import sg.edu.np.madTeam3.assignment.Models.CategoryModel;
import sg.edu.np.madTeam3.assignment.Models.ProjectModel;
import sg.edu.np.madTeam3.assignment.R;
import sg.edu.np.madTeam3.assignment.ViewHolders.ShiftTaskViewHolder;
import sg.edu.np.madTeam3.assignment.Models.TaskModel;
import sg.edu.np.madTeam3.assignment.Utility.DBHandler;

import java.util.ArrayList;
import java.util.List;

public class ShiftTaskAdapter extends RecyclerView.Adapter<ShiftTaskViewHolder> {
    /*
        Author: Zhao Yi
        Sections: All


        ShiftTaskAdapter
        Purpose: To display category in a recycler view in shift task fragment for the user to
                 select the category the task will be shifted to
        Accessed from: Shift task fragment.

        1. Constructor
        2. Create view holder
        3. Bind data to view holder [Line 85]
            Insert the category data to the view holder and attach its various listeners.
            3.1 Select category [Line 98]
                 Change the task's category in the database and proceed to task fragment.
        4. Get Item Count
    */

    //Declare variables
    private ProjectModel projectData;
    private List<CategoryModel> categoryModelList = new ArrayList<>();
    private CategoryModel categoryData;
    private TaskModel taskData;
    private DBHandler dbHandler;
    private Context context;

    //1. Constructor
    public ShiftTaskAdapter(Context context, ProjectModel projectData, TaskModel taskData, DBHandler dbHandler) {
        this.projectData = projectData;
        this.categoryModelList.addAll(projectData.getCategories());
        for (int i = 0; i < categoryModelList.size(); i++) {
            if (categoryModelList.get(i).getCategoryId().equals(taskData.getCategoryId())) {
                this.categoryData = categoryModelList.get(i); //remove the category the task is in
                break;
            }
        }
        categoryModelList.remove(categoryData);
        this.taskData = taskData;
        this.dbHandler = dbHandler;
        this.context = context;
    }

    //2. Create view holder
    @NonNull
    @Override
    public ShiftTaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ShiftTaskViewHolder viewHolder;

        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_select_category,
                parent,
                false
        );

        viewHolder = new ShiftTaskViewHolder(view);
        return viewHolder;
    }

    //3. Bind data to view holder
    @Override
    public void onBindViewHolder(@NonNull ShiftTaskViewHolder holder, int position) {
        //Declare function variables
        CategoryModel category = categoryModelList.get(position);

        //Set data
        String categoryTitle = category.getTitle();
        holder.selectCategoryName.setText(categoryTitle);

        //Set required data as tags for their functions
        holder.selectCategoryName.setTag(category);

        //3.1 Select category
        holder.selectCategoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Declare function variables;
                CategoryModel category = (CategoryModel) v.getTag();

                //Update task category
                dbHandler.updateTask(taskData, category.getCategoryId(), taskData.getTitle(), taskData.getDescription(), taskData.getPriority());

                //Display task fragment
                Bundle bundle = new Bundle();
                bundle.putInt("projectId", projectData.getProjectId()); //Required data to identify project
                ProjectFragment projectObj = new ProjectFragment();
                projectObj.setArguments(bundle);
                ((MainActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        projectObj).commit();

                //Set actionbar title
                ((MainActivity) context).getSupportActionBar().setTitle("Projects");

                //Success message
                Toast.makeText(context, "Task shifted to new category successfully", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (categoryModelList != null) {
            return categoryModelList.size();
        }
        return 0;
    }
}
