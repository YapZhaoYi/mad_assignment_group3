package sg.edu.np.madTeam3.assignment.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import sg.edu.np.madTeam3.assignment.Models.NoteModel;
import sg.edu.np.madTeam3.assignment.ViewHolders.NoteViewHolder;

import sg.edu.np.madTeam3.assignment.R;
import sg.edu.np.madTeam3.assignment.Models.TaskModel;
import sg.edu.np.madTeam3.assignment.Utility.DBHandler;

import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter<NoteViewHolder> {
    /*
        Author: Zhao Yi
        Sections: All


        NoteAdapter
        Purpose: To display notes in a recycler view in task fragment.
        Accessed from: Task fragment.

        1. Constructor
        2. Create view holder
        3. Bind data to view holder [Line 74]
            Insert the note data to the view holder and attach its various listeners.
            3.1 Edit note description [Line 88]
                 Once user exits editing text state, save into database and update category data.
            3.2 Delete note [Line 101]
                 Run a confirmation alert, and delete note from database and task data once
                 confirmed before refreshing the recycler view.
        4. Get Item Count
    */

    //Declare variables
    private TaskModel taskData;
    private List<NoteModel> noteModelList;
    private DBHandler dbHandler;
    private Context context;

    //1. Constructor
    public NoteAdapter(Context context, TaskModel taskData, DBHandler dbHandler) {
        this.taskData = taskData;
        this.noteModelList = taskData.getNotes();
        this.dbHandler = dbHandler;
        this.context = context;
    }

    //2. Create view holder
    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        NoteViewHolder viewHolder;

        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_note,
                parent,
                false
        );
        viewHolder = new NoteViewHolder(view);
        return viewHolder;
    }

    //3. Bind data to view holder
    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
        //Declare function variables;
        NoteModel note = noteModelList.get(position);
        String noteDescription = note.getDescription();

        //Set data
        holder.noteDescription.setText(noteDescription);

        //Set required data as tags for their functions
        holder.noteDescription.setTag(note);
        holder.noteDelete.setTag(note);

        //3.1 Edit note description
        holder.noteDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                //If user exits text editing state.
                if (!hasFocus) {
                    NoteModel note = ((NoteModel) v.getTag());
                    note.setDescription(((EditText) v).getText().toString().trim());
                    dbHandler.updateNote(note, note.getDescription());
                }
            }
        });

        //3.2 Delete note
        holder.noteDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Declare function variables;
                final NoteModel note = ((NoteModel) v.getTag());

                //Confirmation alert
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Delete Note?");
                builder.setMessage("Are you sure you want to permanently delete this note?");
                builder.setCancelable(false); //Closable without an option
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int id) {
                        //Delete note from database and current task data
                        dbHandler.deleteNote(note.getNoteId());
                        taskData.getNotes().remove(note);
                        notifyDataSetChanged(); //Refresh recycler view
                    }

                    ;
                });
                builder.setNegativeButton("No", null);
                builder.show();
            }
        });
    }

    //4 Item count
    @Override
    public int getItemCount() {
        if (noteModelList != null) {
            return noteModelList.size();
        }
        return 0;
    }
}
