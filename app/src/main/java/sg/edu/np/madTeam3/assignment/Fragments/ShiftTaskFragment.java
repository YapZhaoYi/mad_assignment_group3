package sg.edu.np.madTeam3.assignment.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import sg.edu.np.madTeam3.assignment.Activities.MainActivity;
import sg.edu.np.madTeam3.assignment.Adapters.ShiftTaskAdapter;
import sg.edu.np.madTeam3.assignment.Models.CategoryModel;
import sg.edu.np.madTeam3.assignment.Models.ProjectModel;
import sg.edu.np.madTeam3.assignment.R;
import sg.edu.np.madTeam3.assignment.Models.TaskModel;
import sg.edu.np.madTeam3.assignment.Utility.DBHandler;

public class ShiftTaskFragment extends Fragment {
    /*
        Author: Zhao Yi
        Sections: All


        ShiftTaskFragment
        Purpose: To display categories for the user to select, to shift a pre-selected task to.
        Accessed from: Home fragment, project Fragment and task fragment.

        1. OnCreateView
            1.1 Back [Line 85]
                 Proceed to task fragment based on pre-selected task.
    */

    //Declare variables
    private ProjectModel projectData;
    private CategoryModel categoryData;
    private TaskModel taskData;
    private ShiftTaskAdapter adapter;
    private Context context;
    private DBHandler dbHandler;
    private TextView categoryName;
    private View v;

    //1. OnCreateView
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //Set reference to database
        dbHandler = new DBHandler(getActivity(), "PrOrganize.db", null, 1);

        //Get required data
        Bundle bundle = getArguments();
        projectData = dbHandler.getProject(bundle.getInt("projectId"));
        categoryData = dbHandler.getCategory(bundle.getInt("categoryId"));
        taskData = dbHandler.getTask(bundle.getInt("taskId"));

        //Get view
        v = inflater.inflate(R.layout.fragment_shift_task, container, false);

        //Set reference to XML
        categoryName = v.findViewById(R.id.selectCategoryName);
        TextView shiftTaskBack = v.findViewById(R.id.selectCategoryBack);

        //Set data
        context = getActivity();
        categoryName.setText(categoryData.getTitle());

        projectData.setCategories(dbHandler.getAllProjectCategories(projectData));

        //Set up recycler view to display selectable categories
        RecyclerView selectCategoryRecyclerView = v.findViewById(R.id.selectCategoryRecyclerView);
        adapter = new ShiftTaskAdapter(context, projectData, taskData, dbHandler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        selectCategoryRecyclerView.setLayoutManager(layoutManager);
        selectCategoryRecyclerView.setItemAnimator(new DefaultItemAnimator());
        selectCategoryRecyclerView.setAdapter(adapter);

        //1.1 Back
        shiftTaskBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Display task fragment
                Bundle bundle = new Bundle();
                bundle.putInt("projectId", projectData.getProjectId()); //Required data to identify project
                bundle.putInt("categoryId", categoryData.getCategoryId());
                bundle.putInt("taskId", taskData.getTaskId());
                TaskFragment taskObj = new TaskFragment();
                taskObj.setArguments(bundle);
                ((MainActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        taskObj).commit();

                //Set actionbar title
                ((MainActivity) context).getSupportActionBar().setTitle("Task Information");
            }
        });

        return v;
    }
}
