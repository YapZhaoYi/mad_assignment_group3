package sg.edu.np.madTeam3.assignment.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import sg.edu.np.madTeam3.assignment.Activities.MainActivity;
import sg.edu.np.madTeam3.assignment.Adapters.NoteAdapter;
import sg.edu.np.madTeam3.assignment.Models.CategoryModel;
import sg.edu.np.madTeam3.assignment.Models.ProjectModel;
import sg.edu.np.madTeam3.assignment.R;
import sg.edu.np.madTeam3.assignment.Models.TaskModel;
import sg.edu.np.madTeam3.assignment.Utility.DBHandler;
import com.google.android.material.navigation.NavigationView;

public class TaskFragment extends Fragment {
    /*
        Author: Zhao Yi
        Sections: All


        TaskFragment
        Purpose: To display the task's information as well as its related notes.
        Accessed from: Home fragment, project fragment, shift task fragment's back function.

        1. OnCreateView
            1.1 Edit task title [Line 123]
                 Once user exits editing text state, save into database and update project data.
            1.2 Edit task description [Line 135]
                 Once user exits editing text state, save into database and update project data.
            1.3 Delete task [Line 147]
                 Run a confirmation alert, and delete project from database once confirmed before
                 refreshing the navigation view.
            1.4 Add new note [Line 188]
                 Add a note with default information to be edited, into the project data and database
                 before refreshing the recycler view.
            1.5 Back [Line 200]
                 Proceed to project fragment based on pre-selected task.
            1.6 Shift task [Line 217]
                 Proceed to shift task fragment.
            1.7 Change status [Line 243]
                 Changes the status of the task before changing the color of the priority view.
        2. Set color [Line 284]
            Changes the XML background of the priority view. (Placed in this view due to its relevance)
    */

    //Declare variables
    private EditText taskTitle,taskDescription;
    private ProjectModel projectData;
    private CategoryModel categoryData;
    private TaskModel taskData;
    private NavigationView navigationView;
    private NoteAdapter adapter;
    private Context context;
    private DBHandler dbHandler;
    private TextView currentPriority;
    private View v;

    //1. OnCreateView
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //Set reference to database
        dbHandler = new DBHandler(getActivity(), "PrOrganize.db", null, 1);

        //Get required data
        Bundle bundle = getArguments();
        projectData = dbHandler.getProject(bundle.getInt("projectId"));
        categoryData = dbHandler.getCategory(bundle.getInt("categoryId"));
        taskData = dbHandler.getTask(bundle.getInt("taskId"));

        //Get view
        v = inflater.inflate(R.layout.fragment_view_task_information,container,false);

        //Set reference to XML
        taskTitle = v.findViewById(R.id.taskTitle);
        taskDescription = v.findViewById(R.id.taskDescription);
        currentPriority = v.findViewById(R.id.currentPriority);
        navigationView = getActivity().findViewById(R.id.nav_view);
        TextView taskPriorityNone = v.findViewById(R.id.taskPriorityNone);
        TextView taskPriorityNotImpt = v.findViewById(R.id.taskPriorityNotImpt);
        TextView taskPriorityImpt = v.findViewById(R.id.taskPriorityImpt);
        TextView taskPriorityVeryImpt = v.findViewById(R.id.taskPriorityVeryImpt);
        TextView taskBack = v.findViewById(R.id.taskBack);
        ImageView taskAdd = v.findViewById(R.id.taskAdd);
        ImageView taskInfoShift = v.findViewById(R.id.taskInfoShift);
        ImageView taskInfoDelete = v.findViewById(R.id.taskInfoDelete);
        RecyclerView noteRecyclerView = v.findViewById(R.id.noteRecyclerView);

        //Set data
        context = getActivity();
        setColor(currentPriority, taskData.getPriority());
        taskTitle.setText(taskData.getTitle());
        taskDescription.setText(taskData.getDescription());

        projectData.setCategories(dbHandler.getAllProjectCategories(projectData));
        taskData.setNotes(dbHandler.getAllTaskNotes(taskData));

        //Set up recycler view to display notes
        adapter = new NoteAdapter(context,taskData,dbHandler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, true);
        layoutManager.setStackFromEnd(true); //So together with reverseLayout, it will start from the top and load the last item first.

        noteRecyclerView.setLayoutManager(layoutManager);
        noteRecyclerView.setItemAnimator(new DefaultItemAnimator());
        noteRecyclerView.setAdapter(adapter);

        //1.1 Edit task title
        taskTitle.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                //If user exits text editing state.
                if (!hasFocus) {
                    taskData.setTitle(taskTitle.getText().toString().trim());
                    dbHandler.updateTask(taskData,taskData.getCategoryId(),taskData.getTitle(),taskData.getDescription(),taskData.getPriority());
                }
            }
        });

        //1.2 Edit task description
        taskDescription.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                //If user exits text editing state.
                if (!hasFocus) {
                    taskData.setDescription(taskDescription.getText().toString().trim());
                    dbHandler.updateTask(taskData,taskData.getCategoryId(),taskData.getTitle(),taskData.getDescription(),taskData.getPriority());
                }
            }
        });

        //1.3 Delete task
        taskInfoDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Confirmation alert
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Delete Task?");
                builder.setMessage("Are you sure you want to permanently delete this task?");
                builder.setCancelable(false); //closable without an option
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int id) {
                        //Delete task from database
                        dbHandler.deleteTask(taskData.getTaskId());

                        categoryData.getTasks().remove(taskData);

                        //Refresh navigation menu with deleted project
                        if (((MainActivity) context).mPreviousMenuItem != null) {
                            ((MainActivity) context).mPreviousMenuItem.setChecked(false);
                        }
                        ((MainActivity) context).mPreviousMenuItem = navigationView.getMenu().getItem(4).getSubMenu().findItem(categoryData.getProjectId());
                        ((MainActivity) context).mPreviousMenuItem.setChecked(true);

                        //Return to project fragment
                        Bundle bundle = new Bundle();
                        bundle.putInt("projectId", projectData.getProjectId()); //Required data to identify project
                        ProjectFragment projectObj = new ProjectFragment();
                        projectObj.setArguments(bundle);
                        ((MainActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                projectObj).commit();

                        //Set actionbar title
                        ((MainActivity) context).getSupportActionBar().setTitle("Projects");
                    };
                });
                builder.setNegativeButton("No",null);
                builder.show();
            }
        });

        //1.4 Add new note
        taskAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Update current task data
                dbHandler.addNoteToTask(taskData.getTaskId(),"New Note");
                taskData.getNotes().clear();
                taskData.getNotes().addAll(dbHandler.getAllTaskNotes(taskData));
                adapter.notifyDataSetChanged(); //Refresh recycler view
            }
        });

        //1.5 Back
        taskBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Display project fragment
                Bundle bundle = new Bundle();
                bundle.putInt("projectId", projectData.getProjectId()); //Required data to identify project
                ProjectFragment projectObj = new ProjectFragment();
                projectObj.setArguments(bundle);
                ((MainActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        projectObj).commit();

                //Set actionbar title
                ((MainActivity) context).getSupportActionBar().setTitle("Projects");
            }
        });

        //1.6 Shift task
        taskInfoShift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Check if there are other categories to shift to
                if (projectData.getCategories().size() > 1) {
                    //Display shift task fragment
                    Bundle bundle = new Bundle();
                    bundle.putInt("projectId", projectData.getProjectId()); //Required data to identify project
                    bundle.putInt("categoryId", categoryData.getCategoryId());
                    bundle.putInt("taskId", taskData.getTaskId());
                    ShiftTaskFragment taskObj = new ShiftTaskFragment();
                    taskObj.setArguments(bundle);
                    ((MainActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            taskObj).commit();

                    //Set actionbar title
                    ((MainActivity) context).getSupportActionBar().setTitle("Shift task to category");
                }
                else {
                    //Display error message
                    Toast.makeText(context, "No other category to shift to.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //1.7 Change status
        final View.OnClickListener changeStatus = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Update according to what priority color is chosen
                switch (v.getId()) {
                    case R.id.taskPriorityNone:
                        taskData.setPriority("None");
                        dbHandler.updateTask(taskData,taskData.getCategoryId(),taskData.getTitle(),taskData.getDescription(),"None");
                        break;
                    case R.id.taskPriorityNotImpt:
                        taskData.setPriority("NotImpt");
                        dbHandler.updateTask(taskData,taskData.getCategoryId(),taskData.getTitle(),taskData.getDescription(),"NotImpt");
                        break;
                    case R.id.taskPriorityImpt:
                        taskData.setPriority("Impt");
                        dbHandler.updateTask(taskData,taskData.getCategoryId(),taskData.getTitle(),taskData.getDescription(),"Impt");
                        break;
                    case R.id.taskPriorityVeryImpt:
                        taskData.setPriority("VeryImpt");
                        dbHandler.updateTask(taskData,taskData.getCategoryId(),taskData.getTitle(),taskData.getDescription(),"VeryImpt");
                        break;
                }

                //Refresh color of view
                setColor(currentPriority,taskData.getPriority());

                //Display success message
                Toast.makeText(context, "Status updated successfully", Toast.LENGTH_SHORT).show();
            }
        };

        //Assign listener to all priority color views
        taskPriorityNone.setOnClickListener(changeStatus);
        taskPriorityNotImpt.setOnClickListener(changeStatus);
        taskPriorityImpt.setOnClickListener(changeStatus);
        taskPriorityVeryImpt.setOnClickListener(changeStatus);

        return v;
    }

    //2. Set color
    public static void setColor(TextView status, String priority) {
        //Static as it can be accessed by Home fragment and Project fragment without instancing
        switch (priority) {
            case "None":
                status.setBackgroundResource(R.drawable.bg_layer3);
                break;
            case "NotImpt":
                status.setBackgroundResource(R.drawable.bg_notimpt);
                break;
            case "Impt":
                status.setBackgroundResource(R.drawable.bg_impt);
                break;
            case "VeryImpt":
                status.setBackgroundResource(R.drawable.bg_veryimpt);
                break;
        }
    }
}
