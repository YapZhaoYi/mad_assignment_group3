package sg.edu.np.madTeam3.assignment.ViewHolders;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import sg.edu.np.madTeam3.assignment.R;

public class CategoryViewHolder extends RecyclerView.ViewHolder {
    /*
        Author: Zhao Yi
        Sections: All


        CategoryViewHolder
        Purpose: To display a single category in the kanban board.
    */


    public RecyclerView taskRecyclerView;
    public EditText categoryName;
    public ImageView categoryDelete, categoryAdd;

    public CategoryViewHolder(@NonNull View itemView) {
        super(itemView);

        taskRecyclerView = itemView.findViewById(R.id.taskRecyclerView);
        categoryName = itemView.findViewById(R.id.categoryName);
        categoryAdd = itemView.findViewById(R.id.categoryAdd);
        categoryDelete = itemView.findViewById(R.id.categoryDelete);
    }
}
