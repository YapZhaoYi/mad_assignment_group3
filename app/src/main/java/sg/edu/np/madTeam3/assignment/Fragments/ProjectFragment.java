package sg.edu.np.madTeam3.assignment.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import sg.edu.np.madTeam3.assignment.Activities.MainActivity;
import sg.edu.np.madTeam3.assignment.Adapters.CategoryAdapter;
import sg.edu.np.madTeam3.assignment.Models.ProjectModel;
import sg.edu.np.madTeam3.assignment.R;
import sg.edu.np.madTeam3.assignment.Utility.DBHandler;
import sg.edu.np.madTeam3.assignment.Utility.ImageByteConverter;
import com.google.android.material.navigation.NavigationView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

public class ProjectFragment extends Fragment {
    /*
        Author: Zhao Yi
        Sections: All


        ProjectFragment
        Purpose: To display the kanban board.
        Accessed from: The navigation view of main activity as well as deleting a task from task
                       fragment.

        1. OnCreateView
            1.1 Edit project name [Line 118]
                 Once user exits editing text state, save into database and update project data and
                 refresh the navigation view.
            1.2 Change image [Line 136]
                 Proceed to gallery to pick an image to change the project icon in navigation view.
            1.3 Delete project [Line 149]
                 Run a confirmation alert, and delete project from database once confirmed before
                 refreshing the navigation view.
            1.4 Add new category [Line 179]
                 Add a category with default information to be edited, into the project data and database
                 before refreshing the recycler view.
        2. OnActivityResult [Line 193]
            Optimize the image and store into the database and refresh the navigation view.
        3. Update image in fragment [Line 228]
            Updates the image beside the project name in the fragment.
    */

    //Declare variables
    private EditText projectName;
    private ImageView projectIcon;
    private ProjectModel projectData;
    private NavigationView navigationView;
    private CategoryAdapter adapter;
    private Context context;
    private DBHandler dbHandler;
    View v;

    //1. OnCreateView
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //Set reference to database
        dbHandler = new DBHandler(getActivity(), "PrOrganize.db", null, 1);

        //Get required data
        Bundle bundle = getArguments();
        projectData = dbHandler.getProject(bundle.getInt("projectId"));

        //Get view
        v = inflater.inflate(R.layout.fragment_project, container, false);

        //Set reference to XML
        projectName = v.findViewById(R.id.projectName);
        projectIcon = v.findViewById(R.id.projectIcon);
        ImageView changeImage = v.findViewById(R.id.projectImage);
        ImageView deleteProject = v.findViewById(R.id.projectDelete);
        ImageView addCategory = v.findViewById(R.id.projectAdd);
        navigationView = getActivity().findViewById(R.id.nav_view);

        //Set data
        context = getActivity();
        projectName.setText(projectData.getTitle());
        UpdateImage(); //update image in fragment

        projectData.setCategories(dbHandler.getAllProjectCategories(projectData));

        //Set up recycler view to display categories with their tasks
        RecyclerView categoryRecyclerView = v.findViewById(R.id.categoryRecyclerView);
        adapter = new CategoryAdapter(context, projectData, dbHandler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        categoryRecyclerView.setLayoutManager(layoutManager);
        categoryRecyclerView.setItemAnimator(new DefaultItemAnimator());
        categoryRecyclerView.setAdapter(adapter);

        //1.1 Edit project name
        projectName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    //If user exits text editing state
                    projectData.setTitle(projectName.getText().toString().trim());
                    dbHandler.updateProject(projectData, projectData.getTitle(), projectData.getImage());

                    //Refresh navigation menu with updated name
                    Menu menu = navigationView.getMenu();
                    Menu submenu = menu.getItem(4).getSubMenu();
                    submenu.clear();
                    ((MainActivity) context).populateWithProjects();
                }
            }
        });

        //1.2 Change image
        changeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Access Gallery
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");

                startActivityForResult(Intent.createChooser(intent, "Pick project icon"), 1);
                //Results in 2. onActionResult
            }
        });

        //1.3 Delete project
        deleteProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Confirmation alert
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Delete Project?");
                builder.setMessage("Are you sure you want to permanently delete this project?");
                builder.setCancelable(false); //Closable without an option
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int id) {
                        //Delete project from database
                        dbHandler.deleteProject(projectData.getProjectId());
                        Menu menu = navigationView.getMenu();

                        //Refresh navigation menu with deleted project
                        menu.clear();
                        navigationView.inflateMenu(R.menu.drawer_menu);
                        ((MainActivity) context).populateWithProjects();

                        //Return to home page
                        ((MainActivity) context).defaultNavigation();
                    }
                });
                builder.setNegativeButton("No", null);
                builder.show();
            }
        });

        //1.4 Add new category
        addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Update current project data
                dbHandler.addCategoryToProject(projectData.getProjectId(), "New Title");
                projectData.getCategories().clear();
                projectData.getCategories().addAll(dbHandler.getAllProjectCategories(projectData));
                adapter.notifyDataSetChanged(); //Refresh recycler view
            }
        });
        return v;
    }

    //2. onActivityResult
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1) {
            Uri selectedImage = data.getData(); //get image in uri form
            Bitmap bitmap = null;

            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), selectedImage); //convert uri to bitmap
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (bitmap != null) {
                //Optimize image for storage
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true); //reduce the size of the bitmap image
                bitmap.compress(Bitmap.CompressFormat.PNG, 10, out); //reduce the quality of the bitmap image and result in a bitmap stream
                bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray())); //get the bitmap stream and convert back to bitmap

                //Save into database
                projectData.setImage(ImageByteConverter.getBytes(bitmap));  //Convert bitmap to byte array
                dbHandler.updateProject(projectData, projectData.getTitle(), ImageByteConverter.getBytes(bitmap));

                //Update image in fragment
                UpdateImage();

                //Refresh navigation menu with updated project icon
                navigationView.getMenu().clear();
                navigationView.inflateMenu(R.menu.drawer_menu);
                ((MainActivity) getActivity()).populateWithProjects();
            }
        }
    }

    //3. Update image in fragment
    private void UpdateImage() {
        //Check if project has a project icon
        if (projectData.getImage() != null) {
            //If true, set project icon
            projectIcon.setImageDrawable(new BitmapDrawable(getResources(), ImageByteConverter.getImage(projectData.getImage())));
        }
    }
}
