package sg.edu.np.madTeam3.assignment.ViewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import sg.edu.np.madTeam3.assignment.R;

public class TaskViewHolder extends RecyclerView.ViewHolder {
    /*
        Author: Zhao Yi
        Sections: All


        TaskViewHolder
        Purpose: To display a single task under a category in a kanban board.
    */

    public TextView taskName, taskPriority;
    public ConstraintLayout task;
    public ImageView taskDelete,taskShift;

    public TaskViewHolder(@NonNull View itemView) {
        super(itemView);

        taskName = itemView.findViewById(R.id.taskName);
        taskPriority = itemView.findViewById(R.id.taskPriority);
        taskDelete = itemView.findViewById(R.id.taskDelete);
        task = itemView.findViewById(R.id.task);
        taskShift = itemView.findViewById(R.id.taskShift);
    }
}
