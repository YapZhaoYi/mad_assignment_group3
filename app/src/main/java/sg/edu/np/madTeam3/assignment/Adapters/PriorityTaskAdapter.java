package sg.edu.np.madTeam3.assignment.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import sg.edu.np.madTeam3.assignment.Activities.MainActivity;
import sg.edu.np.madTeam3.assignment.Models.CategoryModel;
import sg.edu.np.madTeam3.assignment.Models.ProjectModel;
import sg.edu.np.madTeam3.assignment.R;
import sg.edu.np.madTeam3.assignment.Fragments.ShiftTaskFragment;
import sg.edu.np.madTeam3.assignment.Fragments.TaskFragment;
import sg.edu.np.madTeam3.assignment.Models.TaskModel;
import sg.edu.np.madTeam3.assignment.ViewHolders.TaskViewHolder;
import sg.edu.np.madTeam3.assignment.Utility.DBHandler;

import com.google.android.material.navigation.NavigationView;

import java.util.List;

public class PriorityTaskAdapter extends RecyclerView.Adapter<TaskViewHolder> {
    /*
        Author: Zhao Yi
        Sections: All


        PriorityTaskAdapter
        Purpose: To display tasks in a recycler view in home fragment.
        Accessed from: Home fragment.

        1. Constructor
        2. Create view holder
        3. Bind data to view holder [Line 82]
            Insert the task data to the view holder and attach its various listeners.
            3.1 Open task information [Line 98]
                 Proceed to task fragment.
            3.2 Delete task [Line 132]
                 Run a confirmation alert, and delete task from database once confirmed before
                 refreshing the recycler view.
            3.3 Shift task [Line 160]
                 Proceed to shift task fragment.
        4. Get Item Count
    */

    //Declare variables
    private List<TaskModel> taskModelList;
    private DBHandler dbHandler;
    private Context context;
    private NavigationView navigationView;

    //1. Constructor
    public PriorityTaskAdapter(Context context, List<TaskModel> taskModelList, DBHandler dbHandler) {
        this.taskModelList = taskModelList;
        this.dbHandler = dbHandler;
        this.context = context;
        this.navigationView = ((MainActivity) context).findViewById(R.id.nav_view);
    }

    //2. Create view holder
    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TaskViewHolder viewHolder;

        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_task,
                parent,
                false
        );

        viewHolder = new TaskViewHolder(view);
        return viewHolder;
    }

    //3. Bind data to view holder
    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        //Declare function variables;
        TaskModel task = taskModelList.get(position);

        //Set data
        String taskTitle = task.getTitle();
        holder.taskName.setText(taskTitle);
        TaskFragment.setColor(holder.taskPriority, task.getPriority());

        //Set required data as tags for their functions
        holder.task.setTag(task);
        holder.taskDelete.setTag(task);
        holder.taskShift.setTag(task);

        //3.1 Open task information
        holder.task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Declare function variables;
                TaskModel task = (TaskModel) v.getTag();

                CategoryModel categoryData = dbHandler.getCategory(task.getCategoryId());

                //If there is a previous navigation item
                if (((MainActivity) context).mPreviousMenuItem != null) {
                    ((MainActivity) context).mPreviousMenuItem.setChecked(false); //Uncheck previous navigation item
                }

                //Set reference to current navigation item
                ((MainActivity) context).mPreviousMenuItem = navigationView.getMenu().getItem(4).getSubMenu().findItem(categoryData.getProjectId());

                ((MainActivity) context).mPreviousMenuItem.setChecked(true);

                //Display task fragment
                Bundle bundle = new Bundle();
                bundle.putInt("projectId", categoryData.getProjectId()); //Required data to identify project
                bundle.putInt("categoryId", categoryData.getCategoryId());
                bundle.putInt("taskId", task.getTaskId());
                TaskFragment taskObj = new TaskFragment();
                taskObj.setArguments(bundle);
                ((MainActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        taskObj).commit();

                //Change actionbar title
                ((MainActivity) context).getSupportActionBar().setTitle("Task Information");
            }
        });

        //3.2 Delete task
        holder.taskDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Declare function variables;
                final TaskModel task = (TaskModel) v.getTag();

                //Confirmation alert
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Delete Task?");
                builder.setMessage("Are you sure you want to permanently delete this task?");
                builder.setCancelable(false); //Closable without an option
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int id) {
                        //Delete task from database and current category data
                        dbHandler.deleteTask(task.getTaskId());
                        taskModelList.remove(task);
                        PriorityTaskAdapter.this.notifyDataSetChanged(); //Refresh recycler view
                    }

                    ;
                });
                builder.setNegativeButton("No", null);
                builder.show();
            }
        });

        //3.3 Shift task
        holder.taskShift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Declare function variables;
                TaskModel task = (TaskModel) view.getTag();

                CategoryModel categoryData = dbHandler.getCategory(task.getCategoryId());
                ProjectModel projectData = dbHandler.getProject(categoryData.getProjectId());

                //Check if there are other categories to shift to
                if (dbHandler.getAllProjectCategories(projectData).size() > 1) {
                    //Display shift task fragment
                    Bundle bundle = new Bundle();
                    bundle.putInt("projectId", projectData.getProjectId()); //Required data to identify project
                    bundle.putInt("categoryId", categoryData.getCategoryId());
                    bundle.putInt("taskId", task.getTaskId());

                    ShiftTaskFragment taskObj = new ShiftTaskFragment();
                    taskObj.setArguments(bundle);
                    ((MainActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            taskObj).commit();

                    //Change actionbar title
                    ((MainActivity) context).getSupportActionBar().setTitle("Shift task to category");
                } else {
                    //Display error message
                    Toast.makeText(context, "No other category to shift to.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (taskModelList != null) {
            return taskModelList.size();
        }
        return 0;
    }
}
