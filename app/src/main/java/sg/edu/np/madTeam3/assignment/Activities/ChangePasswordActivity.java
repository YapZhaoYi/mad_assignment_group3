package sg.edu.np.madTeam3.assignment.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import sg.edu.np.madTeam3.assignment.Models.UserModel;
import sg.edu.np.madTeam3.assignment.R;
import sg.edu.np.madTeam3.assignment.Utility.DBHandler;

public class ChangePasswordActivity extends AppCompatActivity {
    /*
        Author: Yu Min
        Sections: All

        Change Password Activity
        Purpose: The purpose of this activity is to change the user's password to a new password
        Accessed from: This activity is accessed from ForgetPassword Activity after they have verified ownership of the account.

        1. OnCreate
            1.1 Change Password [Line 50]
                 A function to check for password strength and that the passwords match, then updates the password in the database.
    */

    //initialise variables
    EditText inputPass;
    EditText cfmPass;
    Button btnConfirm;

    //Set reference to Database
    private DBHandler dbHandler = new DBHandler(this, "PrOrganize.db", null, 1);

    //1. OnCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        //find ids in view
        inputPass = findViewById(R.id.inputPass);
        cfmPass = findViewById(R.id.cfmPass);
        btnConfirm = findViewById(R.id.btnConfirm);

        //1.1 Change Password
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputPassText = inputPass.getText().toString().trim();
                String cfmPassText = cfmPass.getText().toString().trim();

                //check if inputpass is empty
                if (TextUtils.isEmpty(inputPassText)){
                    inputPass.setError("Please enter a password");
                    return;
                }

                //check if cfm pass is empty
                if (TextUtils.isEmpty(cfmPassText)){
                    inputPass.setError("Please re-enter your password");
                    return;
                }

                //check if inputpass is at least 6 characters
                if (inputPassText.length() < 6) {
                    inputPass.setError("Password must be at least 6 characters.");
                    return;
                }

                //check if passwords match
                if (!inputPassText.equals(cfmPassText)){
                    cfmPass.setError("Passwords do not match");
                    return;
                }

                //update password
                String username = getIntent().getExtras().getString("username");
                String email = getIntent().getExtras().getString("email");
                UserModel userData = dbHandler.getUser(username);
                dbHandler.updateUser(userData, inputPassText, email);

                //show success toast
                Toast.makeText(ChangePasswordActivity.this, "Password Successfully Changed!", Toast.LENGTH_SHORT).show();

                //redirect back to login
                Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
