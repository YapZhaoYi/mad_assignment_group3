package sg.edu.np.madTeam3.assignment.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import sg.edu.np.madTeam3.assignment.Activities.MainActivity;
import sg.edu.np.madTeam3.assignment.Fragments.ShiftTaskFragment;
import sg.edu.np.madTeam3.assignment.Models.CategoryModel;
import sg.edu.np.madTeam3.assignment.Models.ProjectModel;
import sg.edu.np.madTeam3.assignment.R;
import sg.edu.np.madTeam3.assignment.Fragments.TaskFragment;
import sg.edu.np.madTeam3.assignment.Models.TaskModel;
import sg.edu.np.madTeam3.assignment.ViewHolders.TaskViewHolder;
import sg.edu.np.madTeam3.assignment.Utility.DBHandler;

import java.util.List;

public class TaskAdapter extends RecyclerView.Adapter<TaskViewHolder> {
    /*
        Author: Zhao Yi
        Sections: All


        TaskAdapter
        Purpose: To display tasks in a nested recycler view in project fragment to form the Kanban
                 board.
        Accessed from: Project fragment.

        1. Constructor
        2. Create view holder
        3. Bind data to view holder [Line 84]
            Insert the task data to the view holder and attach its various listeners.
            3.1 Open task information [Line 100]
                 Proceed to task fragment.
            3.2 Delete task [Line 122]
                 Run a confirmation alert, and delete task from database once confirmed before
                 refreshing the recycler view.
            3.3 Shift task [Line 150]
                 Proceed to shift task fragment.
        4. Get Item Count
    */

    //Declare variables
    private ProjectModel projectData;
    private CategoryModel categoryData;
    private List<TaskModel> taskModelList;
    private DBHandler dbHandler;
    private Context context;

    //1. Constructor
    public TaskAdapter(Context context, ProjectModel projectData, CategoryModel categoryData, DBHandler dbHandler) {
        this.projectData = projectData;
        this.categoryData = categoryData;
        this.taskModelList = categoryData.getTasks();
        this.dbHandler = dbHandler;
        this.context = context;
    }

    //2. Create view holder
    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TaskViewHolder viewHolder;

        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_task,
                parent,
                false
        );

        viewHolder = new TaskViewHolder(view);
        return viewHolder;
    }

    //3. Bind data to view holder
    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        //Declare function variables;
        TaskModel task = taskModelList.get(position);

        //Set data
        String taskTitle = task.getTitle();
        holder.taskName.setText(taskTitle);
        TaskFragment.setColor(holder.taskPriority, task.getPriority());

        //Set required data as tags for their functions
        holder.task.setTag(task);
        holder.taskDelete.setTag(task);
        holder.taskShift.setTag(task);

        //3.1 Open task information
        holder.task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Declare function variables;
                TaskModel task = (TaskModel) v.getTag();

                //Display task fragment
                Bundle bundle = new Bundle();
                bundle.putInt("projectId", projectData.getProjectId()); //Required data to identify project
                bundle.putInt("categoryId", categoryData.getCategoryId());
                bundle.putInt("taskId", task.getTaskId());
                TaskFragment taskObj = new TaskFragment();
                taskObj.setArguments(bundle);
                ((MainActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        taskObj).commit();

                //Change actionbar title
                ((MainActivity) context).getSupportActionBar().setTitle("Task Information");
            }
        });

        //3.2 Delete task
        holder.taskDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Declare function variables;
                final TaskModel task = (TaskModel) v.getTag();

                //Confirmation alert
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Delete Task?");
                builder.setMessage("Are you sure you want to permanently delete this task?");
                builder.setCancelable(false); //Closable without an option
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int id) {
                        //Delete task from database and current category data
                        dbHandler.deleteTask(task.getTaskId());
                        categoryData.getTasks().remove(task);
                        TaskAdapter.this.notifyDataSetChanged();
                    }

                    ;
                });
                builder.setNegativeButton("No", null);
                builder.show();
            }
        });

        //3.3 Shift task
        holder.taskShift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Declare function variables;
                TaskModel task = (TaskModel) view.getTag();

                //Check if there are other categories to shift to
                if (dbHandler.getAllProjectCategories(projectData).size() > 1) {
                    //Display shift task fragment
                    Bundle bundle = new Bundle();
                    bundle.putInt("projectId", projectData.getProjectId()); //Required data to identify project
                    bundle.putInt("categoryId", categoryData.getCategoryId());
                    bundle.putInt("taskId", task.getTaskId());

                    ShiftTaskFragment taskObj = new ShiftTaskFragment();
                    taskObj.setArguments(bundle);
                    ((MainActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            taskObj).commit();

                    //Change actionbar title
                    ((MainActivity) context).getSupportActionBar().setTitle("Shift task to category");
                } else {
                    //Display error message
                    Toast.makeText(context, "No other category to shift to.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (taskModelList != null) {
            return taskModelList.size();
        }
        return 0;
    }
}
