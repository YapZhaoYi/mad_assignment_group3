package sg.edu.np.madTeam3.assignment.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import sg.edu.np.madTeam3.assignment.BuildConfig;
import sg.edu.np.madTeam3.assignment.Models.UserModel;
import sg.edu.np.madTeam3.assignment.R;
import sg.edu.np.madTeam3.assignment.Utility.DBHandler;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class ForgetPasswordActivity extends AppCompatActivity {
    /*
        Author: Yu Min
        Sections: All

        Forget Password Activity
        Purpose: The purpose of this activity is to verify that the user owns the email by sending them a random-generated code,
                 then redirect them to the ChangePassword activity.
        Accessed from: This activity is accessed from Login Activity register function

        1. OnCreate
            1.1 Email Code to user[Line 85]
                 A function to get user's email based on their username, then send a 6-digit code to their email
            1.2 Confirm Code [Line 144]
                 A function to check that the user received the code and direct them to change password page
            1.3 Return to Login [Line 174]
                 A function to return user to login page
        2. Send email using SendGrid API [Line 185]
            A function to send an email to specified email address using SendGrid API
        3. Check for internet connectivity [Line 220]
            A function to check whether the user is connected to the internet
    */

    //initialise variables
    EditText inputUsername;
    Button btnGetCode;
    EditText inputCode;
    Button btnConfirm;
    int randomSixDigitCode;
    String email;
    String username;
    TextView returnToLogin;
    UserModel userData;

    //Set reference to Database
    private DBHandler dbHandler = new DBHandler(this, "PrOrganize.db", null, 1);

    //1. OnCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        //remove need for async
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //find ids for elements
        inputUsername = findViewById(R.id.inputUsername);
        btnGetCode = findViewById(R.id.btnGetCode);
        inputCode = findViewById(R.id.inputCode);
        btnConfirm = findViewById(R.id.btnConfirm);
        returnToLogin = findViewById(R.id.returnLogIn);

        //1.1 Email Code to user
        btnGetCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = inputUsername.getText().toString().trim(); //the trim is to remove whitespaces at the end

                //check if username field is empty
                if(TextUtils.isEmpty(username)){
                    inputUsername.setError("Please enter your username");
                    return;
                }

                //check if username exists in database
                if (!dbHandler.validateUser(username)){
                    inputUsername.setError("Account doesn't exist");
                    return;
                }

                //get user's data
                userData = dbHandler.getUser(username);
                email = userData.getEmail();

                //generate code
                final int min = 100000;
                final int max = 999999;
                randomSixDigitCode = new Random().nextInt((max - min) + 1) + min;
                Log.v("TAG", String.valueOf(randomSixDigitCode));

                //check for internet connection
                Log.v("TAG", "Network Connection? : " + String.valueOf(isNetworkConnected()));
                if (!isNetworkConnected()){
                    Toast.makeText(ForgetPasswordActivity.this, "No internet connection. Please try again later.", Toast.LENGTH_SHORT).show();
                    return;
                }

                //send email to user using SendGrid API
                try {
                    int responseCode = emailCode(email, randomSixDigitCode);

                    //if request returns as error
                    if (String.valueOf(responseCode).charAt(0) == '4'){
                        Toast.makeText(ForgetPasswordActivity.this, "Error sending email. Please try again later.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //prompt user to check email
                AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPasswordActivity.this);
                builder.setTitle("Check your email!");
                builder.setMessage("You should have received a 6-digit-code from us. If you don't see it, try checking your spam folder.");
                builder.show();

                inputCode.setVisibility(View.VISIBLE);
                btnConfirm.setVisibility(View.VISIBLE);
            }
        });

        //1.2 Confirm Code
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String code = inputCode.getText().toString().trim(); //trim to remove any whitespaces at the front or back

                //check if code field is empty
                if(TextUtils.isEmpty(code)){
                    inputCode.setError("Please enter the code");
                    return;
                }

                //check if code is correct
                if (!code.equals(String.valueOf(randomSixDigitCode))) {
                    inputCode.setError("Incorrect code");
                    return;
                }

                //show success toast
                Toast.makeText(ForgetPasswordActivity.this, "Correct Code Entered", Toast.LENGTH_SHORT).show();

                //redirect user to change password
                Intent intent = new Intent(ForgetPasswordActivity.this, ChangePasswordActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("email", email);
                startActivity(intent);
                finish();
            }
        });

        //1.3 Return to Login
        returnToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    //2 Send email using SendGrid API. Returns a (int) http response code.
    private int emailCode(String email, int code) throws IOException {
        final String apiKey = BuildConfig.SENDGRID_API_KEY;
        Log.v("TAG", "API key = " + apiKey);

        URL url = new URL("https://api.sendgrid.com/v3/mail/send");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        int responseCode;
        try {
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + apiKey);

            String body = "{\"personalizations\": [{\"to\": [{\"email\": \"" + email + "\"}]}]," +
                    "\"from\": {\"email\": \"prorganisecompany@gmail.com\"}," +
                    "\"subject\": \"PrOrganise Password Reset\"," +
                    "\"content\": [{\"type\": \"text/plain\", " +
                    "\"value\": \"Your 6-digit code is: " + code + "\"}]}";

            Log.v("TAG", body);

            byte[] bodyInBytes = body.getBytes("UTF-8");
            OutputStream os = new BufferedOutputStream(connection.getOutputStream());
            os.write(bodyInBytes);
            os.close();
            Log.v("TAG", "outputstream written");
            responseCode = connection.getResponseCode();
            Log.v("TAG", "Reponse:" + responseCode);
        } finally{
            connection.disconnect();
        }
        return responseCode;
    }

    //3. Check for network connectivity
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}
