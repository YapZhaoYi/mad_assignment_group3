package sg.edu.np.madTeam3.assignment.Utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ImageByteConverter {
    /*
        Author: Zhao Yi
        Sections: All


        ImageByteConvertor
        Purpose: To represent the data of notes added to a task during the process of doing a task.
        Accessed from: Main Activity and Project fragment add project icon.

        1. Get image [Line 25]
            Convert byte array to bitmap
        2. Get byte array [Line 30]
            Convert bitmap to byte array
    */

    //1. Get image
    public static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    //2. Get byte
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream); //Do not further reduce quality after initial quality compress.
        try {
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream.toByteArray();
    }
}
