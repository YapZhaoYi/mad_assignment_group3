package sg.edu.np.madTeam3.assignment.ViewHolders;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import sg.edu.np.madTeam3.assignment.R;

public class NoteViewHolder extends RecyclerView.ViewHolder {
    /*
        Author: Zhao Yi
        Sections: All


        NoteViewHolder
        Purpose: To display a single note in the task recycler view.
    */

    public EditText noteDescription;
    public ImageView noteDelete;

    public NoteViewHolder(@NonNull View itemView) {
        super(itemView);

        noteDescription = itemView.findViewById(R.id.noteDescription);
        noteDelete = itemView.findViewById(R.id.noteDelete);
    }
}
