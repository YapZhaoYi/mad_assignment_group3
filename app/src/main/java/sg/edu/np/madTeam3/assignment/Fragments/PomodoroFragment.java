package sg.edu.np.madTeam3.assignment.Fragments;


import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import sg.edu.np.madTeam3.assignment.R;

public class PomodoroFragment extends Fragment {
/*
        Author: Jason Teo
        Sections: All

        PomodoroFragment
        Purpose: To count down from either 5 or 25 minutes so that the user can
                 be more productive in their work
        Accessed from: The navigation view of main activity

        1. OnCreateView
            1.1 Start button [Line 86]
                A function to start the timer and change visibility of start, reset and back button.
            1.2 Stop button [Line 101]
                A function to stop the timer and change visibility of start, reset and back button.
            1.3 Reset button [Line 112]
                A function to reset the timer.
        2. ChooseTimer [Line 123]
            Check which timer it is on and switch it to either 5 or 25 min.
        3. StopTimer [Line 128]
            Used to stop the timer.
        4. StartTimer [Line 144]
            Get timer counter value and validate values to make sure no values displayed are zero.
        5. CheckMinutes [Line 175]
            Check if timer counter value is equals to X amount of minutes.
        6. CheckSeconds [Line 181]
            Returns the amount of seconds that is not used to form the minutes.
    */

    //Declare Variables
    private TextView timer;
    private Button timerStartButton;
    private Button timerStopButton;
    private Button timerResetButton;
    private boolean timerReset;
    private long timerValue;
    public int counter;
    private CountDownTimer ctTimer;

    @Nullable
    @Override

    //1. OnCreateView
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Get the view
        View view = inflater.inflate(R.layout.fragment_pomodoro, container, false);

        //Assigns initial values to variables as well as linking the buttons and text views
        timerValue = 5 * 60000; //initial value is 5 min
        timer = view.findViewById(R.id.timer);
        timerStartButton = view.findViewById(R.id.StartButton);
        timerStopButton = view.findViewById(R.id.StopButton);
        timerResetButton = view.findViewById(R.id.ResetButton);
        timerReset = true;

        int counterMin = checkMinute((int) timerValue / 1000);
        timer.setText(String.valueOf(counterMin) + " min");

        //1.1 Start Button
        timerStartButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (timerReset) {
                    counter = (int) timerValue / 1000;
                }
                startTimer();
                timerStartButton.setVisibility(View.INVISIBLE);
                timerResetButton.setVisibility(View.INVISIBLE);
                timerStopButton.setVisibility(View.VISIBLE);

            }
        });

        //1.2 Stop button
        timerStopButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                stopTimer();
                timerStartButton.setVisibility(View.VISIBLE);
                timerResetButton.setVisibility(View.VISIBLE);
                timerStopButton.setVisibility(View.INVISIBLE);
            }
        });

        //1.3 On Click for the Reset button
        timerResetButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                timerReset = true;
                timer.setText(String.valueOf(timerValue / 60000) + " min");
            }
        });
        return view;
    }

    //2. ChooseTimer
    private long chooseTimer(long timerValue) {
        if (timerValue == 5 * 60000) {
            timer.setText("25 min");
            return 25 * 60000;
        } else if (timerValue == 25 * 60000) {
            timer.setText("5 min");
            return 5 * 60000;
        } else {
            return timerValue;
        }
    }

    //3. StopTimer
    private void stopTimer() {
        ctTimer.cancel();
        timerReset = false;
    }

    //4. StartTimer
    private void startTimer() {
        //Assigns new CountDownTimer
        ctTimer = new CountDownTimer(timerValue, 1000) {
            public void onTick(long millTillFinished) {
                int counterMin = checkMinute(counter);
                int counterSec = checkSeconds(counterMin, counter);
                //Check if Minutes is zero
                if (counterMin == 0) {
                    timer.setText(String.valueOf(counterSec) + " s");
                }
                //Check if Seconds is zero
                else if (counterSec == 0) {
                    timer.setText(String.valueOf(counterMin) + " min");
                } else {
                    timer.setText(String.valueOf(counterMin) + " min " + String.valueOf(counterSec) + "s");
                }
                counter--;
            }

            //Reset everything and calls chooseTimer
            public void onFinish() {
                timerReset = true;
                timerValue = chooseTimer(timerValue);
                timerStartButton.setVisibility(View.VISIBLE);
                timerResetButton.setVisibility(View.VISIBLE);
                timerStopButton.setVisibility(View.INVISIBLE);
            }
        }.start();
    }

    //5. CheckMinutes
    private int checkMinute(int counter) {
        int minutes = counter / 60;
        return minutes;
    }

    //6. CheckSeconds
    private int checkSeconds(int minutes, int counter) {
        int seconds = counter - (minutes * 60);
        return seconds;
    }
}
