package sg.edu.np.madTeam3.assignment.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import sg.edu.np.madTeam3.assignment.R;
import sg.edu.np.madTeam3.assignment.Models.UserModel;
import sg.edu.np.madTeam3.assignment.Utility.DBHandler;

public class RegisterActivity extends AppCompatActivity {
    /*
        Author: Delbert
        Sections: 1.1 (Validation), 1.2

        Author: Zhao Yi
        Section 1.1 (Create New User)

        Login Activity
        Purpose: To register an account
        Accessed from: This activity is accessed from Login Activity register function

        1. OnCreate
            1.1 Register [Line 56]
                 A register function with basic validation and verification of input data,
                 and directs the user to the main activity.
            1.2 Return to login [Line 118]
                 A function to direct the user back to the login activity.
    */

    //Declare variables
    private EditText inputName, inputEmail, inputPass, cfmPass;

    //Set reference to Database
    private DBHandler dbHandler = new DBHandler(this, "PrOrganize.db", null, 1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Set reference to XML
        inputName = findViewById(R.id.inputName);
        inputEmail = findViewById(R.id.inputEmail);
        inputPass = findViewById(R.id.inputPass);
        cfmPass = findViewById(R.id.cfmPass);
        Button buttonReg = findViewById(R.id.btnReg);
        TextView returnLogin = findViewById(R.id.returnLogIn);

        //1.1 Register
        buttonReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Declare function variables
                String name, email, pass, pass2;

                //Get inputted text
                name = inputName.getText().toString().trim();
                email = inputEmail.getText().toString().trim();
                pass = inputPass.getText().toString().trim();
                pass2 = cfmPass.getText().toString().trim();

                //Validation
                //Check if username is empty
                if (TextUtils.isEmpty(name)) {
                    inputName.setError("Name is required.");
                    return; //Exit function if true
                }

                //check if username already exists
                if (dbHandler.validateUser(name)) {
                    inputName.setError("Username already in use");
                    return; //Exit function if true
                }

                //Check if email is empty
                if (TextUtils.isEmpty(email)) {
                    inputEmail.setError("Email is required.");
                    return; //Exit function if true
                }

                //Check if password is empty
                if (TextUtils.isEmpty(pass)) {
                    inputPass.setError("Password is required.");
                    return; //Exit function if true
                }

                //Check if password is lesser than 6 characters
                if (pass.length() < 6) {
                    inputPass.setError("Password must be at least 6 characters.");
                    return; //Exit function if true
                }

                //Check if the two passwords match
                if (!pass2.equals(pass)) {
                    cfmPass.setError("Password do not match.");
                    return; //Exit function if false
                }

                //Create new user
                UserModel userData = new UserModel(name, pass, email);
                dbHandler.addUser(userData); //Add to database

                //Proceed to Main Activity
                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                intent.putExtra("userData", name); //Required data to identify current user
                startActivity(intent);
                finish();
            }
        });

        //1.2 Return to login
        returnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Proceed to Login Activity
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }
}
