package sg.edu.np.madTeam3.assignment.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import sg.edu.np.madTeam3.assignment.Adapters.PriorityTaskAdapter;
import sg.edu.np.madTeam3.assignment.R;
import sg.edu.np.madTeam3.assignment.Models.UserModel;
import sg.edu.np.madTeam3.assignment.Utility.DBHandler;

public class HomeFragment extends Fragment {
    /*
        Author: Zhao Yi
        Sections: All


        HomeFragment
        Purpose: To display a welcome message and very important priority tasks for the user's
                 convenience while helping them remember certain tasks.
        Accessed from: Login activity, register activity, default main activity fragment, deleting
                       of project, on back pressed in main activity as well as the navigation view
                       of main activity.

        1. OnCreateView
            Display priority tasks in recycler view.
    */

    //Declare variables
    private PriorityTaskAdapter adapter;
    private Context context;
    private DBHandler dbHandler;
    View v;

    //1. OnCreateView
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //Set reference to database
        dbHandler = new DBHandler(getActivity(), "PrOrganize.db", null, 1);

        //Get required data
        Bundle bundle = getArguments();
        UserModel userData = dbHandler.getUser(bundle.getString("username"));

        //Get view
        v = inflater.inflate(R.layout.fragment_home, container, false);

        //Set reference to XML
        TextView userText = v.findViewById(R.id.userText);

        //Set data
        context = getActivity();
        userText.setText(userData.getName());

        //Set up recycler view to display priority tasks
        RecyclerView priorityTaskRecyclerView = v.findViewById(R.id.priorityTaskRecyclerView);

        adapter = new PriorityTaskAdapter(context, dbHandler.getAllPriorityTask(userData), dbHandler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        priorityTaskRecyclerView.setLayoutManager(layoutManager);
        priorityTaskRecyclerView.setItemAnimator(new DefaultItemAnimator());
        priorityTaskRecyclerView.setAdapter(adapter);

        return v;
    }
}
